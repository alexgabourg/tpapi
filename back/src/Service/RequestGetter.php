<?php
/**
 * Created by PhpStorm.
 * User: wille
 * Date: 16/01/2019
 * Time: 16:47
 */

namespace App\Service;


class RequestGetter
{
    public function get_request(&$callback,$request, $parameters, $optionals = []){
        $retour = [];
        $req = json_decode($request->getContent());
        foreach ($parameters as $parameter){
            if (isset($req->$parameter) && $req->$parameter != ""){
                $retour[$parameter] = $req->$parameter;
            } else {
                return false;
            }
        }
        foreach ($optionals as $parameter){
            if (isset($req->$parameter) && $req->$parameter != ""){
                $retour[$parameter] = $req->$parameter;
            } else {
                $retour[$parameter] = null;
            }
        }
        $callback = $retour;
        return true;
    }
}
