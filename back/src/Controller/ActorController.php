<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 18/01/2019
 * Time: 10:24
 */

namespace App\Controller;
use App\Entity\Actor;
use App\Service\RequestGetter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ActorController extends AbstractController
{
    function getAllActors() {
//        if (false === isset($_GET['token']) || $_GET['token'] !== 'IsJohn') {
//            $response = ["error" => "Unauthorized"];
//            $responseJson = json_encode($response);
//            return new Response($responseJson, 401);
//        }
        $actorsentities = $this->getDoctrine()->getRepository(Actor::class)->findAll();
        $actors = [];
        foreach ($actorsentities as $actorentity) {
            $actors[] = [
                "id" => $actorentity->getId(),
                "firstName" => $actorentity->getFirstName(),
                "lastName" => $actorentity->getLastName(),


            ];
        }
        return new JsonResponse($actors, 200 ,['Access-Control-Allow-Origin' =>"*"]);
    }
    function getActor(Actor $actor) {
        $movies = [];
        foreach ($actor->getMovies() as $movie) {
            $movies[] = [
                'id' => $movie->getId(),
                "title" => $movie->getTitle(),
                "poster" => $movie->getPoster()
            ];
        }
        $actor= [
            "id" => $actor->getId(),
            "firstName" => $actor->getFirstName(),
            "lastName" => $actor->getLastName(),
            "movies" => $movies

        ];
        return new JsonResponse($actor, 200, ['Access-Control-Allow-Origin' => "*"]);
    }

    function createActor(Request $request, RequestGetter $getter){
        $actor = new Actor();
        $em = $this->getDoctrine()->getManager();

//        dump($request->getContent()->firstName);
        if ($getter->get_request($param, $request, ['firstName', 'lastName'])) {
            $actor->setFirstName($param['firstName']);
            $actor->setLastName($param['lastName']);

            $em->persist($actor);
            $em->flush();
        } else {
            return new JsonResponse('Invalid input', 405,['Access-Control-Allow-Origin' =>"*"]);
        }
        return new JsonResponse('actor created', 201,['Access-Control-Allow-Origin' =>"*"]);
    }

    function updateActor(Actor $actor,Request $request, RequestGetter $getter){
        $em = $this->getDoctrine()->getManager();

        if ($getter->get_request($param, $request, ['firtName', 'lastName'])) {
            $actor->setFirstName($param['firtName']);
            $actor->setLastName($param['lastName']);

            $em->persist($actor);
            $em->flush();
        } else {
            return new JsonResponse('Invalid input', 405,['Access-Control-Allow-Origin' =>"*"]);
        }
        return new JsonResponse('actor updated successfully', 201,['Access-Control-Allow-Origin' =>"*"]);
    }

    function deleteActor(Actor $actor){
        $em = $this->getDoctrine()->getManager();
        $em->remove($actor);
        $em->flush();
        return new JsonResponse('actor deleted successfully', 204,['Access-Control-Allow-Origin' =>"*"]);
    }
}
