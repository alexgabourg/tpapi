<?php

namespace App\Controller;

use App\Entity\Actor;
use App\Entity\Categories;
use App\Entity\Movies;
use App\Service\RequestGetter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class MovieController extends AbstractController
{
    function getAllMovies()
    {
////        if (false === isset($_GET['token']) || $_GET['token'] !== 'IsJohn') {
//            $response = ["error" => "Unauthorized"];
//            $responseJson = json_encode($response);
//            return new Response($responseJson, 401);
////        }
        $moviesentities = $this->getDoctrine()->getRepository(Movies::class)->findAll();
        $movies = [];
        foreach ($moviesentities as $movieentity) {
            $movies[] = [
                "id" => $movieentity->getId(),
                "title" => $movieentity->getTitle(),
                "year" => $movieentity->getYear(),
                "poster" => $movieentity->getPoster(),
                "synopsis" => $movieentity->getSynopsis()
            ];
        }

        return new JsonResponse($movies, 200, ['Access-Control-Allow-Origin' => "*"]);
    }

    function createMovie(Request $request, RequestGetter $getter)
    {
        $movie = new Movies();
        $em = $this->getDoctrine()->getManager();


        if ($getter->get_request($param, $request, ['title'], ['synopsis', 'poster', 'year', 'actors', 'category'])) {
            $actors = [];
            foreach ($param['actors'] as $actor){
                $actors[] = $this->getDoctrine()->getRepository(Actor::class)->find($actor);
            }
            $movie->setTitle($param['title']);
            $movie->setYear($param['year']);
            $movie->setPoster($param['poster']);
            $movie->setActors($actors);
            $movie->setCategory($this->getDoctrine()->getRepository(Categories::class)->find($param['category']));
            $movie->setSynopsis($param['synopsis']);
            $em->persist($movie);
            $em->flush();
        } else {
            return new JsonResponse('Invalid input', 405,['Access-Control-Allow-Origin' =>"*"]);
        }
        return new JsonResponse('movie created', 201,['Access-Control-Allow-Origin' =>"*"]);
//        return new JsonResponse($movie, 200, ['Access-Control-Allow-Origin' => "*"]);
    }


    function updateMovie(Movies $movie, Request $request, RequestGetter $getter)
    {
        $em = $this->getDoctrine()->getManager();

        if ($getter->get_request($param, $request, ['title'], ['synopsis', 'poster', 'year', 'actor', 'category'])) {
            $movie->setTitle($param['title']);
            $movie->setYear($param['year']);
            $movie->setPoster($param['poster']);
            $movie->setActors($param['actor']);
            $movie->setCategory($param['category']);
            $movie->setSynopsis($param['synopsis']);
            $em->persist($movie);
            $em->flush();
        } else {
            return new JsonResponse('Invalid input', 405,['Access-Control-Allow-Origin' =>"*"]);
        }
        return new JsonResponse('movie updated successfully', 204,['Access-Control-Allow-Origin' =>"*"]);
//        return new JsonResponse($movie, 200, ['Access-Control-Allow-Origin' => "*"]);
    }

    function getMovie(Movies $movie)
    {
        $actors = [];
        foreach ($movie->getActors() as $actor) {
            $actors[] = [
                'id' => $actor->getId(),
                "firstName" => $actor->getFirstName(),
                "lastName" => $actor->getLastName()
            ];
        }
        $category = null;
        if($movie->getCategory()){
            $category = [
                'id' => $movie->getCategory()->getId(),
                'name' => $movie->getCategory()->getName()
            ];
        }
        $movie = [
            "id" => $movie->getId(),
            "title" => $movie->getTitle(),
            "year" => $movie->getYear(),
            "poster" => $movie->getPoster(),
            "synopsis" => $movie->getSynopsis(),
            "actors" => $actors,
            "category" => $category
        ];

        return new JsonResponse($movie, 200, ['Access-Control-Allow-Origin' => "*"]);
    }

    function deleteMovie(Movies $movie)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($movie);
        $em->flush();
        return new JsonResponse('movie deleted successfully', 204,['Access-Control-Allow-Origin' =>"*"]);
//        return new JsonResponse($movie, 200, ['Access-Control-Allow-Origin' => "*"]);
    }
}
