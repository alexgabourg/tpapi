<?php

namespace App\Controller;
use App\Entity\Categories;
use App\Service\RequestGetter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategoryController extends AbstractController
{
    function getAllCategories() {
//        if (false === isset($_GET['token']) || $_GET['token'] !== 'IsJohn') {
//            $response = ["error" => "Unauthorized"];
//            $responseJson = json_encode($response);
//            return new Response($responseJson, 401);
//        }
        $categorysentities = $this->getDoctrine()->getRepository(Categories::class)->findAll();
        $categories = [];
        foreach ($categorysentities as $categoriesentity) {
            $categories[] = [
                "id" => $categoriesentity->getId(),
                "name" => $categoriesentity->getName()];
        }
        return new JsonResponse($categories, 200 ,['Access-Control-Allow-Origin' =>"*"]);
    }

    function getCategory(Categories $category) {
        $movies = [];
        foreach ($category->getMovies() as $movie) {
            $movies[] = [
                'id' => $movie->getId(),
                "title" => $movie->getTitle(),
                "poster" => $movie->getPoster(),
            ];
        }
        $category= [
            "id" => $category->getId(),
            "name" => $category->getName(),
            "movies" => $movies
        ];
        return new JsonResponse($category, 200, ['Access-Control-Allow-Origin' => "*"]);
    }

    function createCategory(Request $request, RequestGetter $getter){
        $category = new Categories();
        $em = $this->getDoctrine()->getManager();

        if ($getter->get_request($param, $request, ['name'])) {
            $category->setName($param['name']);

            $em->persist($category);
            $em->flush();
        } else {
            return new JsonResponse('Invalid input', 405,['Access-Control-Allow-Origin' =>"*"]);
        }
        return new JsonResponse('category created', 201,['Access-Control-Allow-Origin' =>"*"]);
//        return new JsonResponse($movie, 200, ['Access-Control-Allow-Origin' => "*"]);
    }

    function updateCategory(Categories $category, Request $request, RequestGetter $getter){
        $em = $this->getDoctrine()->getManager();

        if ($getter->get_request($param, $request, ['name'])) {
            $category->setName($param['name']);

            $em->persist($category);
            $em->flush();

        } else {
            return new JsonResponse('Invalid input', 405,['Access-Control-Allow-Origin' =>"*"]);
        }
        return new JsonResponse('category updated successfully', 204,['Access-Control-Allow-Origin' =>"*"]);
    }

    function deleteCategory(Categories $category){
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();
        return new JsonResponse('category deleted successfully', 204,['Access-Control-Allow-Origin' =>"*"]);
    }
}
