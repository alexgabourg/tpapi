import Vue from 'vue'
import Router from 'vue-router'
import Films from '@/components/Films'
import Film from '@/components/Film'
import CategoryEdit from '@/components/CategoryEdit'
import FilmEdit from '../components/FilmEdit'
import Actors from '../components/Actors'
import ActorEdit from '../components/ActorEdit'
import Actor from '../components/Actor'
import Index from '../components/Index'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/films',
      name: 'films',
      component: Films
    },
    {
      path: '/film/add',
      name: 'filmadd',
      component: FilmEdit,
    },
    {
      path: '/film/edit/:id',
      name: 'filmEdit',
      component: FilmEdit,
      props: true
    },
    {
      path: '/film/:id',
      name: 'film',
      component: Film,
      props: true
    },
    {
      path: '/category/add',
      name: 'categoryAdd',
      component: CategoryEdit,
      props: true
    },
    {
      path: '/category/edit/:id',
      name: 'categoryEdit',
      component: CategoryEdit,
      props: true
    },
    {
      path: '/actors',
      name: 'actors',
      component: Actors
    },
    {
      path: '/actor/add',
      name: 'actorAdd',
      component: ActorEdit,
    },
    {
      path: '/actor/edit/:id',
      name: 'actorEdit',
      component: ActorEdit,
      props: true
    },
    {
      path: '/actor/:id',
      name: 'actor',
      component: Actor,
      props: true
    },

  ]
})
